const { fx, app, dispatch } = require('slurm/frontend');
const table = require('./table');

const state = {}

const actions = {
  echo: value => (state, actions) =>
    dispatch("echo",
             value,
             "override",
             "error"),
  
  override: response => state => ({ response }),
  error: error => state => ({ error })
}

const hello = 
  ['h1', { className: "pa1" }, [
    ['span', { className: "f-headline-c f1 bg-black-60 washed-green pa1" }, "Hello World"]
  ]];

const showState = state =>
  ["h2", { className: "f2 pa3 gray bg-light-green" }, JSON.stringify(state)];

const button = actions =>
  ['a', {
    className: "f2 no-underline dib br1 bg-dark-green grow washed-green pa3 pointer",
    onclick: () => actions.echo({ msg: "Hallo Welt!" })
  }, "Echo"];

const view = (state, actions) => 
  ['div', { className: "bg-washed-green mid-gray vh-100 w-100 dt tc" }, [
    hello,
    showState(state),
    button(actions),
    table([{ name: 'wurst' }])
  ]];

app(state, view, actions);
