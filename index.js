const slurm = require('slurm');
 
const fn = slurm.dispatcher.generator;

const echo = fn("echo",
                (payload, res) => {
                  res(payload);
                });

slurm.db.initialize(slurm.db.errors.INMEMORY);
slurm.dispatcher.addFunctions([echo]);

slurm.core.startServer(3000, () => console.log("Server startet on port 3000"));
