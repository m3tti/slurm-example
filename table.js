const { pipe, map, keys, toUpper, values, head, append, __ } = require('ramda');

const table = array => 
  ['table', {}, [
    ...th(array),
    ...tr(array)
  ]];

const appendEl = el => append(__, [el, {}]);

const td = appendEl('td');

const tr = map(pipe(values,
                    map(td),
                    appendEl('tr')));

const th = pipe(head,
                keys,
                map(toUpper),
                map(appendEl('th')));

module.exports = table;
